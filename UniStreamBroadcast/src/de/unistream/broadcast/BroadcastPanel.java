package de.unistream.broadcast;

import java.awt.*;
import java.awt.image.*;

import javax.swing.*;

import de.unistream.broadcast.net.BroadcastConnection;
import de.unistream.draw.*;

public class BroadcastPanel
		extends JPanel
{
	
	private BroadcastPanel auto = this;
	private BroadcastClient window;
	private BufferedImage img;
	private Graphics2D g2;
	
	private BroadcastConnection cn = new BroadcastConnection();
	
	/**
	 * Create the panel.
	 */
	public BroadcastPanel(BroadcastClient w, int width, int height)
	{
		// etabliert die Verbindung
		cn.start();
		
		// speichert Referenz auf das Fenster
		window = w;
		this.setBackground(Color.RED);
		setBounds(0, 0, width, height);
		setMinimumSize(new Dimension(width, height));
		img = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		g2 = img.createGraphics();
		g2.fillRect(0, 0, img.getWidth(), img.getHeight());
		
		// Anti-Aliasing einschalten
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		this.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		
		Pencil p = new Pencil(g2, cn.getParser());
		
		this.addMouseListener(p.getMouseListener());
		this.addMouseMotionListener(p.getMouseMotionListener());
		
		new Thread() {
			@Override public void run()
			{
				while (auto.isVisible()) {
					auto.repaint();
					try {
						Thread.sleep(17);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
	
	@Override public void paintComponent(Graphics g)
	{
		g.drawImage(img, 0, 0, null);
	}
	
	public Graphics2D getCanvasGraphics()
	{
		return g2;
	}

}
