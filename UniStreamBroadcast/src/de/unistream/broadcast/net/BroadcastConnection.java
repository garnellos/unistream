package de.unistream.broadcast.net;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

import de.unistream.net.Connection;
import de.unistream.xml.Parser;

public final class BroadcastConnection
		extends Connection
{
	protected Thread connectionThread;
	protected Parser parser;
	
	private boolean alive  = false;
	private boolean paused = false;
	
	public BroadcastConnection()
	{
		parser = new Parser();
		connectionThread = new Thread()
		{
			@Override public void run()
			{
				System.out.println("Establishing broadcast connection.");
				InetAddress serverIP;
				Socket clientSock;
				try {
					serverIP = InetAddress.getByName(null);
					clientSock = new Socket(serverIP, 23556);
					
					if (!clientSock.isConnected()) {
						clientSock.close();
						throw new RuntimeException("no server at " + serverIP.toString() + ":23556");
					}
//					BufferedReader in  = new BufferedReader(new InputStreamReader(clientSock.getInputStream()));
					BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSock.getOutputStream()));
					
					while (alive) {
						while (paused);
						
						if (parser.hasNewData()) {
							System.out.println(parser.peekText());
							out.append(parser.popText());
							out.flush();
						}
						
						Thread.sleep(200);
					}
					clientSock.shutdownOutput();
					clientSock.close();
				} catch (Exception e) {
					System.err.println("Error while broadcasting <in BroadcastConnection.connectionThread.run()>.");
					e.printStackTrace();
//					System.exit(-1);
				}
			}
		};
	}
	
	public Parser getParser()
	{ return this.parser; }
	
	public void start()
	{
		if (paused) paused = false;
		if (!alive) connectionThread.start();
		alive = true;
	}
	
	public void pause()
	{
		if (alive) paused = true;
	}
	
	public void stop()
	{
		paused = false;
		alive  = false;
	}
}
