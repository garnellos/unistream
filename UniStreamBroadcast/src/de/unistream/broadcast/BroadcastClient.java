package de.unistream.broadcast;

import java.awt.*;
import javax.swing.*;
import javax.swing.UIManager.*;

public class BroadcastClient
{

	private JFrame broadcasterClientFrame;
	private BroadcastPanel canvas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		// legt als Look and Feel Nimbus fest (sofern verfügbar).
		// Native LaFs unterstützen InternalFrames nicht zuverlässig.
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, fall back to cross-platform
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (Exception ex) {
				// not worth my time
			}
		}
		
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try {
					BroadcastClient app = new BroadcastClient();
					app.broadcasterClientFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BroadcastClient()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		broadcasterClientFrame = new JFrame();
		broadcasterClientFrame.setBounds(200, 200, 800, 600);
		broadcasterClientFrame.setBackground(Color.BLACK);
		broadcasterClientFrame.setTitle("UniStream Broadcaster Client - verbunden mit 127.0.0.1:23556~0");
		broadcasterClientFrame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		broadcasterClientFrame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JInternalFrame internalFrame = new JInternalFrame("127.0.0.1:23556~0 •LIVE");
		internalFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		internalFrame.setClosable(true);
		internalFrame.setResizable(false);
		internalFrame.setMaximizable(false);
		internalFrame.setIconifiable(true);
		//internalFrame.setBounds(42, 43, 434, 208);
		panel.add(internalFrame);
		
		canvas = new BroadcastPanel(this, 640, 480);
		internalFrame.getContentPane().add(canvas, BorderLayout.CENTER);
		//frame.setContentPane(canvas);
		canvas.setBackground(Color.RED);
		canvas.setLayout(null);
		
		internalFrame.pack(); // passt Größe automatisch an Canvas an
		internalFrame.setVisible(true);
		
		JMenuBar menuBar = new JMenuBar();
		broadcasterClientFrame.getContentPane().add(menuBar, BorderLayout.NORTH);
		
		JMenu mStream = new JMenu("Stream");
		menuBar.add(mStream);
		
		JMenu mDatei = new JMenu("Datei");
		menuBar.add(mDatei);
		
		JMenu mAnsicht = new JMenu("Ansicht");
		menuBar.add(mAnsicht);
		
		JMenu mBearbeiten = new JMenu("Bearbeiten");
		menuBar.add(mBearbeiten);
		
		JMenuBar statusBar = new JMenuBar();
		broadcasterClientFrame.getContentPane().add(statusBar, BorderLayout.SOUTH);
		
		JLabel lblStatus = new JLabel("Bereit.");
		statusBar.add(lblStatus);
		
		broadcasterClientFrame.setVisible(true);
	}
}
