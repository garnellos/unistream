# README #
Mit __UniStream__ können Präsentationen, Vorlesungen u.Ä. direkt vom Referenten auf die Laptops der Zuhörer übertragen werden.

# Funktionen

__UniStream__ besteht aus drei einzelnen Anwendungen:

- einem __Client__, der das _Senden von Inhalten_ ermöglicht (Broadcaster)
- einem __Client__, der den _Empfang von Inhalten_ ermöglicht (Receiver)
- einem __Server__, der die Inhalte vom Broadcaster an die Receiver _verteilt_

![](http://www.garnellos.de/dl/schema.png)

Die Verbindung zwischen _Broadcaster_ und _Receiver_ findet auf dem Server über sog. __Sockets__ statt. Jeder Port des Servers kann mehrere Sockets hosten, wodurch sich die Kapazität eines Servers stark erhöht.

__UniStream__ verfügt über eigene Tools zum Erstellen von Präsentationen. Abgehaltene Präsentationen können auch gespeichert und lokal weiterverarbeitet oder archiviert werden. Zusätzlich können die Zuhörer in Echtzeit Nachrichten an den Referenten senden, um z.B. Fragen zu äußern.