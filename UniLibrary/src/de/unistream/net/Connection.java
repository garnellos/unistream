package de.unistream.net;

/**
 * Repräsentiert eine Verbindung mit einem Socket auf einem Server.
 * @author Philipp Terstappen
 * 
 */
public abstract class Connection
{
	public abstract void start();
	public abstract void pause();
	public abstract void stop();
}
