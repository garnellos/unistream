package de.unistream.xml.tags;

public abstract class ParaTag<T>
		implements Tag
{
	
	@Override public String getXML()
	{ return "<" + getName() + " v=\"" + getValue().toString() + "\" />"; }
	
	public abstract T getValue();
	
}
