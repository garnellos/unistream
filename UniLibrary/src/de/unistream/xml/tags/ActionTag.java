package de.unistream.xml.tags;

import java.util.List;

public abstract class ActionTag
		implements Tag
{
	protected boolean closed = false;
	protected volatile int pointer = 0;
	private StringBuilder sb;
	
	public abstract List<? extends ParaTag<?>> getParameters();
	
	public synchronized String getXML()
	{
		sb = new StringBuilder();
		sb.append("<" + getName() + ">");
		for (ParaTag<?> p : getParameters())
			sb.append(p.getXML());
		if (closed) sb.append("</" + getName() + ">");
		sb.append("\n");
		return sb.toString();
	}
	
	public synchronized void addParameter(ParaTag<?> p) {
		if (closed)
			throw new UnsupportedOperationException("Tag wurde bereits geschlossen.");
	}
	
	// XXX Sollte es möglich sein einen Parameter wieder zu entfernen? Kann mit der Stream-Mechanik kollidieren
//	public abstract void removeParameter(ParaTag<?> p);
	
	public synchronized String peekXML() {
		return getXML().substring(pointer);
	}
	
	public synchronized String popXML() {
		String rv = peekXML();
		pointer = getXML().length() - 1;
		return rv;
	}
	
	public void resetXMLPointer() {
		pointer = 0;
	}
	
	public synchronized void close() {
		closed = true;
	}
	
	public boolean isClosed() {
		return closed;
	}
}
