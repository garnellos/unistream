package de.unistream.xml.tags.action;

import java.util.*;

import de.unistream.xml.tags.ActionTag;
import de.unistream.xml.tags.ParaTag;
import de.unistream.xml.tags.para.Point;

public final class Stroke
		extends ActionTag
{
	
	protected volatile LinkedList<Point> pList;
	
	public Stroke()
	{
		pList = new LinkedList<Point>();
	}
	
	public String getName()
	{ return "stroke"; }
	
	@Override public synchronized List<? extends ParaTag<?>> getParameters()
	{
		return pList;
	}
	
	@Override public synchronized void addParameter(ParaTag<?> p)
	{
		super.addParameter(p);
		
		if (p instanceof Point)
			pList.add((Point) p);
		else
			throw new IllegalArgumentException("Parameter muss ein Point-Objekt sein.");
	}
	
	/*
	@Override public void removeParameter(ParaTag<?> p)
	{
		if (p instanceof Point)
			pList.removeIf((Point ps) -> (ps == p));
	}
	*/
	
}
