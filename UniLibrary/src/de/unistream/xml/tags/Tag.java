package de.unistream.xml.tags;

/**
 * Interface für den grundlegenden Aufbau von Tags im XML-Stream von UniStream.
 * @see ActionTag
 * @see ParaTag
 * @author Philipp Terstappen
 */
public interface Tag
		extends java.io.Serializable
{
	public String getName();
	public String getXML();
}
