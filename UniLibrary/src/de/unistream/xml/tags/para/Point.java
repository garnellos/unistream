package de.unistream.xml.tags.para;

import de.unistream.xml.tags.ParaTag;

/**
 * Klasse für Parametertag Point, abgekürzt {@code <pt>}.
 * @author Philipp Terstappen
 */
public final class Point
		extends ParaTag<java.awt.Point>
{
	
	private java.awt.Point value;
	
	public Point(java.awt.Point p)
	{
		value = p;
	}
	
	@Override public java.awt.Point getValue()
	{
		return value;
	}
	
	@Override public String getName()
	{
		return "pt";
	}
	
	@Override public String getXML()
	{
		return "<pt v=\"" + value.x + "," + value.y + "\" />";
	}
	
}
