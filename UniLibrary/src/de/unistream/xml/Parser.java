package de.unistream.xml;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;

import de.unistream.xml.tags.*;

/**
 * Ermöglicht die Aneinanderreihung mehrerer {@link ActionTag}-Objekte und deren Zusammenfassung zu einem Stream.
 * @author Philipp Terstappen
 */
public final class Parser
{
	protected volatile Deque<ActionTag> tags;
	protected volatile StringBuilder str;
	protected Thread parserThread;
	
	private volatile boolean alive = true;
	private int ptr = 0;
	
	public Parser()
	{
		tags = new LinkedBlockingDeque<ActionTag>();
		str = new StringBuilder();
		parserThread = new Thread()
		{
			@Override public void run()
			{
				ActionTag prev;
				String a;
				while (alive && tags.isEmpty());
				while (alive) {
					prev = tags.getLast();
					while (true) {
						a = prev.popXML();
						if (!a.isEmpty()) str.append(a);
						if (prev.isClosed()) {
							synchronized (prev) {
								str.append(prev.popXML());
							}
							break;
						}
					}
					while (prev == tags.getLast());
				}
			}
		};
		parserThread.start();
	}
	
	public boolean hasNewData()
	{ return str.length() - 1 > ptr; }
	
	public synchronized String peekText()
	{ return str.substring(ptr); }
	
	public synchronized String popText()
	{
		String s = peekText();
		ptr = str.length() - 1;
		return s;
	}
	
	public String getText()
	{ return str.toString(); }
	
	public void kill()
	{ alive = false; }
	
	public boolean isAlive()
	{ return alive; }
	
	public void addAction(ActionTag a)
	{ tags.add(a); }
	
	public ActionTag getLast()
	{ return tags.getLast(); }
}
