package de.unistream.draw;

import java.awt.*;
import java.awt.event.*;

/**
 * Ein {@code Tool} stellt ein auswählbares Werkzeug dar, mit dem das auf der Leinwand sichtbare Bild bearbeitet werden
 * kann.<p>
 * Jedes Tool kann auf <em>Tastatureingaben</em> mit einem {@link KeyListener}, <em>Mausereignisse</em> mit einem
 * {@link MouseListener} und <em>Mausbewegungen</em> mit einem {@link MouseMotionListener} reagieren.
 * @author Philipp Terstappen
 */
public abstract class Tool
{
	
	// KONSTRUKTOR //
	
	/**
	 * Standardroutine zur Erstellung eines Tools. Jedes {@link Tool} benötigt ein {@link Graphics2D}-Objekt, das sich
	 * auf die zu bezeichnende Leinwand bezieht.
	 * @param g2
	 */
	public Tool(Graphics2D g2)
	{
		this.graphics = g2;
	}
	
	// STATISCHE OBJEKTE //
	
	/**
	 * Ein leerer {@link MouseListener}, der standardmäßig von allen Tools zur Verfügung gestellt wird. Wird
	 * {@link #getMouseListener()} nicht überschrieben, ist dieses {@link Tool} nicht in der Lage, auf "interessante"
	 * Maus-Events (klicken, drücken, loslassen, eingehen und ausgehen) zu reagieren.
	 * @see MouseListener
	 * @see #getMouseListener()
	 */
	protected static final MouseListener voidMouseListener = new MouseListener()
	{
		@Override public void mouseReleased(MouseEvent e) {}
		@Override public void mousePressed(MouseEvent e) {}
		@Override public void mouseExited(MouseEvent e) {}
		@Override public void mouseEntered(MouseEvent e) {}
		@Override public void mouseClicked(MouseEvent e) {}
	};
	
	/**
	 * Ein leerer {@link MouseMotionListener}, der standardmäßig von allen Tools zur Verfügung gestellt wird. Wird
	 * {@link #getMouseMotionListener()} nicht überschrieben, ist dieses {@link Tool} nicht in der Lage, auf Bewegungen
	 * der Maus zu reagieren.
	 * @see MouseMotionListener
	 * @see #getMouseMotionListener()
	 */
	protected static final MouseMotionListener voidMouseMotionListener = new MouseMotionListener()
	{
		@Override public void mouseMoved(MouseEvent e) {}
		@Override public void mouseDragged(MouseEvent e) {}
	};
	
	/**
	 * Ein leerer {@link KeyListener}, der standardmäßig von allen Tools zur Verfügung gestellt wird. Wird
	 * {@link #getKeyListener()} nicht überschrieben, ist dieses {@link Tool} nicht in der Lage, auf Tastatureingaben zu
	 * reagieren.
	 */
	protected static final KeyListener voidKeyListener = new KeyListener()
	{
		@Override public void keyTyped(KeyEvent e) {}
		@Override public void keyReleased(KeyEvent e) {}
		@Override public void keyPressed(KeyEvent e) {}
	};
	
	// BEZUGSOBJEKTE //
	
	/**
	 * Das {@link Graphics2D}-Objekt der momentan bezeichneten Leinwand. 
	 */
	protected Graphics2D graphics;
	
	
	// GETTER UND SETTER //
	
	/**
	 * @return Den {@link MouseListener} des Tools. Wenn die Methode nicht überschrieben wurde, wird
	 * {@link #voidMouseListener} zurückgegeben.
	 */
	public MouseListener getMouseListener()
	{ return voidMouseListener; }
	
	/**
	 * @return Den {@link MouseMotionListener} des Tools. Wenn die Methode nicht überschrieben wurde, wird
	 * {@link #voidMouseMotionListener} zurückgegeben.
	 */
	public MouseMotionListener getMouseMotionListener()
	{ return voidMouseMotionListener; }
	
	/**
	 * @return Den {@link KeyListener} des Tools. Wenn die Methode nicht überschrieben wurde, wird
	 * {@link #voidKeyListener} zurückgegeben.
	 */
	public KeyListener getKeyListener()
	{ return voidKeyListener; }
	
	/**
	 * @return {@link #graphics}
	 */
	public final Graphics2D getGraphics()
	{ return graphics; }
	
	/**
	 * @param graphics Das {@link Graphics2D}-Objekt der neuen zu bezeichnenden Leinwand.
	 */
	public final void setGraphics(Graphics2D graphics)
	{
		if (graphics == null)
			throw new IllegalArgumentException("Graphics2D-Objekt eines Tools darf nicht null sein.");
		this.graphics = graphics;
	}
	
}
