package de.unistream.draw;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.*;

import de.unistream.xml.Parser;
import de.unistream.xml.tags.action.Stroke;

/**
 * Die Klasse {@code Pencil} stellt einen einfachen Stift dar. Mit ihr kann freihand gezeichnet werden. Außer der
 * Farbe lassen sich keine weiteren Einstellungen vornehmen. Die Strichstärke beträgt immer {@code 1px}.
 * @author Philipp Terstappen
 */
public class Pencil
		extends Tool
{
	
	protected Point lastClick;
	protected Stroke lastActionTag;
	protected Parser parser;
	
	/**
	 * Erstellt ein neues {@link Pencil}-Objekt. Als Farbe wird {@link java.awt.Color#BLACK Color.BLACK} festgelegt.
	 * @param g2 Das zu bezeichnende {@link Graphics2D}-Objekt.
	 */
	public Pencil(Graphics2D g2, Parser p)
	{
		this(g2, p, Color.BLACK);
	}
	
	/**
	 * Erstellt ein neues {@link Pencil}-Objekt.
	 * @param g2 Das zu bezeichnende {@link Graphics2D}-Objekt.
	 * @param c Die Farbe des Stifts. Kann später mit {@link #setColor(Color)} geändert werden.
	 */
	public Pencil(Graphics2D g2, Parser p, Color c)
	{
		super(g2);
		this.parser = p;
		setColor(c);
	}
	
	/**
	 * Der {@link MouseListener} der Klasse {@link Pencil}. Er beendet das Fortsetzen einer Freihandlinie, sobald
	 * die primäre Maustaste losgelassen wird.
	 */
	@Override public MouseListener getMouseListener()
	{
		return new MouseListener()
		{
			@Override public void mouseReleased(MouseEvent e)
			{
				if (e.getButton() == MouseEvent.BUTTON1) {
					lastClick = null;
					lastActionTag.close();
//					msg.append("</stroke>");
				}
			}
			
			@Override public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					lastActionTag = new Stroke();
					parser.addAction(lastActionTag);
				}
//					msg.append("<stroke>");
			}
			
			@Override public void mouseExited(MouseEvent e) {}
			@Override public void mouseEntered(MouseEvent e) {}
			@Override public void mouseClicked(MouseEvent e) {}
		};
	}
	
	@Override public MouseMotionListener getMouseMotionListener()
	{
		return new MouseMotionListener()
		{
			
			@Override public void mouseMoved(MouseEvent e) {}
			
			@Override public void mouseDragged(MouseEvent e)
			{
				if (e.getButton() == MouseEvent.BUTTON1) {
					Point prevClick = lastClick;
					lastClick = new Point(e.getX(), e.getY());
					
					boolean check = prevClick != null && !prevClick.equals(lastClick);
					if (check)
						draw(prevClick.x, prevClick.y, lastClick.x, lastClick.y);
					if (prevClick == null || (check))
						lastActionTag.addParameter(new de.unistream.xml.tags.para.Point(lastClick));
//						msg.append("<pt l='" + lastClick.x + "," + lastClick.y + "' />");
				}
			}
		};
	}
	
	
	// GETTER UND SETTER //
	
	/**
	 * @return Die Farbe des zu bezeichnenden {@link Graphics2D}-Objekts.
	 */
	public Color getColor()
	{ return graphics.getColor(); }
	
	/**
	 * Legt die (Vordergrund-)Farbe des zu bezeichnenden {@link Graphics2D}-Objekts fest.
	 * @param c Instanz von {@link Color}.
	 */
	public void setColor(Color c)
	{ graphics.setColor(c); }
	
	
	// METHODEN //
	
	/**
	 * Zeichnet eine Linie vom Punkt ({@code x1}|{@code y1}) zum Punkt ({@code x2}|{@code y2}).
	 */
	public void draw(int x1, int y1, int x2, int y2)
	{
		graphics.drawLine(x1, y1, x2, y2);
	}
	
}
