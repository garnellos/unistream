package de.unistream.server;

import java.io.*;
import java.net.*;

public final class Server
{

	public static void main(String[] args)
	{
		
		System.out.println("Server gestartet.");
		try {
			ServerSocket server = new ServerSocket(23556);
			System.out.println("UniStream™ Server läuft auf Port 23556");
			
			Socket sock = server.accept();
			System.out.println("Verbindung zum Client wird aufgebaut.");
			
			BufferedReader in  = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
			
//			StringBuffer daten = new StringBuffer();
			while (true) {
				String tmp = in.readLine();
				
				if (sock.isClosed()) break;
				if (tmp != null && !tmp.isEmpty()) System.out.print(tmp);
			}
			
//			System.out.println("Nachricht vom Client: \n" + daten.toString());
			
			out.write("<check />");
			out.flush();
			sock.shutdownOutput();
			sock.close();
			System.out.println("Verbindung zum Client beendet.");
		} catch (Exception e) {
			System.out.println("Fehler:");
			e.printStackTrace();
		}
		System.out.println("Server wird gestoppt.");
		
	}

}
